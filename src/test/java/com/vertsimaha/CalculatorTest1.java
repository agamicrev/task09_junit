package com.vertsimaha;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;

public class CalculatorTest1 {
  private CalculatorService service;
  private Calculator calculator;

  @Rule
  public ExpectedException expected = ExpectedException.none();

  @BeforeClass
  public static void setUpClass() throws Exception{
    System.out.println("Method would be executed just once");
  }

  @Before
  public void setUp(){
    System.out.println("Method before the test");
    service = Mockito.mock(CalculatorService.class);
    calculator = new Calculator(service);
  }

  @Test
  public void testPlus(){
    System.out.println("Inside testPlus test");
    calculator.plus(Calculator.ZERO, Calculator.ONE);
    Mockito.verify(service).plus(0,1);
  }

  @Test
  public void testDivideThrowsException(){
    System.out.println("Inside testDivide test");
    expected.expect(ArithmeticException.class);
    calculator.divide(10, 0);
  }

}
