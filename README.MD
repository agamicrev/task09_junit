PROJECT DESCRIPTION        

This is a project about functionality of unit testing.        

Task 1.         
Create some sample class. Write a test class for it that use:      
- different asserts  
- static final constant with package-private or protected access level  
- mocks and Mockito.verify   

Task 2.      
Write tests for testing void methods, e.g. file creation  
(that is not unit test actually).      

Task 3.      
Create test suite and run it.      


OUTPUTS        

**************************************        
Task 1.      
Method would be executed just once  
Method before the test  
Inside testPlus test  
Method before the test  
Inside testDivide test    

**************************************        
Task 2.        
Method would be executed just once  
Method before the test  
Inside void method test  

**************************************        
Task 3.        
Method would be executed just once  
Method before the test  
Inside testPlus test  
Method before the test  
Inside testDivide test  
Method would be executed just once  
Method before the test  
Inside void method test  
true  
